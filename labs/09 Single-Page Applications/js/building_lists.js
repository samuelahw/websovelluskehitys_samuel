
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;


/* lista toteutus
var list = document.createElement('ul');
for (var i=0; i < books.length; i++) {
	console.log(books[i].title);
	var item = document.createElement('li');
	item.innerHTML = books[i].title + "  " +  books[i].year;
	list.appendChild(item);
}
document.body.appendChild(list);
 */

let tablekirja = document.createElement('h1');
let table = document.createElement('table');
let sarake = document.createElement('tr');
let teka = document.createElement('th');
let ttoka = document.createElement('th');
teka.innerHTML = "Kirjan nimi";
ttoka.innerHTML = "Vuosi";
sarake.appendChild(teka);
sarake.appendChild(ttoka);
table.appendChild(sarake);
document.body.appendChild(tablekirja);


for (let i=0; i < books.length; i++){
	let sarake = document.createElement('tr');
	let kirja = books[i].title;
	let vuosi = books[i].year;
	let teka = document.createElement('td');
	let ttoka = document.createElement('td');
	teka.innerHTML = kirja;
	ttoka.innerHTML = vuosi;
	sarake.appendChild(teka);
	sarake.appendChild(ttoka);
	table.appendChild(sarake);
}

document.getElementsByTagName('th').headers = "kirjan nimi";
document.body.appendChild(table);

function addRowHandlers() {
	var rows = table.getElementsByTagName("tr");
	for (i = 0; i < rows.length; i++) {
		var currentRow = table.rows[i];
		var createClickHandler = function(row) {
			return function() {
				var cell = row.getElementsByTagName("td")[0];
				var id = cell.innerHTML;
				tablekirja.innerHTML = id;
			};
		};
		currentRow.onclick = createClickHandler(currentRow);
	}
}
window.onload = addRowHandlers();


