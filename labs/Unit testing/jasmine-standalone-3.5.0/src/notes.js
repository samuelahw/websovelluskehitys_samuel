var notes = (function() {
    var list = [];

    return {
        add: function(note) {
            if (note) {
                var item = {timestamp: Date.now(), text: note};
                list.push(item);
                return true;
            }
            return false;
        },
        remove: function(index) {

            if(list.splice(1,1)){
                return true;
            }
            return false;
        },
        count: function() {
            return list.length;
        },
        list: function() {},
        find: function(str) {
            for(let alkio of list){
                if (alkio.text === str){
                    return true;
                }
            }
            return false;
        },
        clear: function() {
            list.splice(0, list.length);
        }
    }
}());


